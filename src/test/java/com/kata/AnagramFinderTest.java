package com.kata;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class AnagramFinderTest {

    @Test
    void should_return_anagram() {
        AnagramFinder sut = new AnagramFinder();
        List<String> words = Arrays.asList("gare","rage");
        sut.setWords(words);

        Map<String,List<String>> result = new HashMap<>();
        result.put("gare", Collections.singletonList("rage"));

        assertThat(result).isEqualTo(sut.getAnagram().get(0));
    }

    @Test
    void should_return_all_anagram() {
        List<String> words = Arrays.asList("gare","rage","egar","test");
        AnagramFinder sut = new AnagramFinder(words);


        Map<String,List<String>> result = new HashMap<>();
        result.put("gare", Arrays.asList("rage","egar"));

        assertThat(result).isEqualTo(sut.getAnagram().get(0));
    }

    @Test
    void should_return_zero_anagram() {
        List<String> words = Arrays.asList("gare","toupi","tapis","test");
        AnagramFinder sut = new AnagramFinder(words);

        assertThat(new ArrayList<>()).isEqualTo(sut.getAnagram());
    }
}