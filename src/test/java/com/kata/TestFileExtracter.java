package com.kata;

import com.kata.utils.FileExtracter;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.assertj.core.api.Assertions.assertThat;

public class TestFileExtracter {
    @Test
    void should_return_words_in_list() {
        FileExtracter sut = new FileExtracter(new File("words.txt"));
        assertThat(1633).isEqualTo(sut.extract().size());
    }
}
