package com.kata;

import com.kata.utils.CommandLineExtractor;
import com.kata.utils.FileExtracter;

import java.io.*;
import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        System.out.println("ANAGRAM KATA");
        /*File file;
        if(args.length != 0){
            file = new File(args[0]);
        }
        else{
            file = new File("words.txt");
        }

        FileExtracter fileExtracter = new FileExtracter(file);
        AnagramFinder anagramFinder = new AnagramFinder(fileExtracter.extract());
        List<Map<String,List<String>>> output = anagramFinder.getAnagram();

        for (Map<String,List<String>> o : output) {
            System.out.println(o);
        }*/

        CommandLineExtractor commandLineExtractor = new CommandLineExtractor();
        List<String> words = commandLineExtractor.extract();
        AnagramFinder anagramFinder = new AnagramFinder(words);
        List<Map<String,List<String>>> output = anagramFinder.getAnagram();

        for (Map<String,List<String>> o : output) {
            System.out.println(o);
        }

    }
}