package com.kata.utils;

import java.util.List;

public interface DataExtractable {
    List<String> extract();
}
