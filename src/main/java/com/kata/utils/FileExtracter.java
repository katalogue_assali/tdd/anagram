package com.kata.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileExtracter implements DataExtractable {
    private File file;
    private String sep;
    private static final String DEFAULT_SEP = " ";
    public FileExtracter(File file) {
        this.file = file;
    }

    public FileExtracter(File file, String sep) {
        this.file = file;
        this.sep = sep;
    }

    public List<String> extract() {
        List<String> words = new ArrayList<>();
        try {
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            String line;
            String sep = getSep();
            while ((line = br.readLine()) != null) {
                for (String s : line.split(sep)) {
                    if (!s.isEmpty()) {
                        words.add(s);
                    }
                }
            }
            fr.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return words;
    }

    private String getSep() {
        if(sep==null) return DEFAULT_SEP;
        return sep;
    }
}

