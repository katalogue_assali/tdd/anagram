package com.kata.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CommandLineExtractor implements DataExtractable {
    @Override
    public List<String> extract() {
        Scanner scanner = new Scanner(System.in);
        List<String> words = new ArrayList<>();
        String input = scanner.nextLine();
        for (String s : input.split( " ")) {
            if (!s.isEmpty()) {
                words.add(s);
            }
        }

        return words;
    }
}
