package com.kata;

import java.util.*;

public class AnagramFinder {
    private List<String> words;

    public AnagramFinder() {
    }

    public AnagramFinder(List<String> words) {
        this.words = words;
    }

    public List<Map<String,List<String>>> getAnagram(){
        List<Map<String,List<String>>> result = new ArrayList<>();
        String key = "";
        List<String> values = new ArrayList<>();
        for(int i = 0; i < words.size();i++){
            key = words.get(i);
            for (String word:words) {
                if (isAnagram(key,word)) {
                    values.add(word);
                }
            }
            Map<String,List<String>> entry = new HashMap<>();
            entry.put(key,values);
            result.add(entry);
            values = new ArrayList<>();
        }
        return getOnlyAnagram(result);
    }

    private List<Map<String, List<String>>> getOnlyAnagram(List<Map<String, List<String>>> input) {
        List<Map<String, List<String>>> result = new ArrayList<>();
        for (Map<String,List<String>> o :input) {
            for (Map.Entry<String, List<String>> pair : o.entrySet()) {
                if(pair.getValue().size()>0) result.add(o);
            }
        }
        return result;
    }

    private boolean isAnagram(String key, String word) {
        if(key.length()!=word.length())return false;
        if(key.equals(word))return false;
        char[] s1= key.toLowerCase().toCharArray();
        char[] s2= word.toLowerCase().toCharArray();

        Arrays.sort(s1);
        Arrays.sort(s2);

        return Arrays.equals(s1,s2);
    }
    public void setWords(List<String> words) {
        this.words = words;
    }
}
